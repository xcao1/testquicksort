// ================================================ //
//           CS570 User Defined Vector              //
//                  Xinlei Cao                      //
// ================================================ //
#ifndef __MY_VECTOR_H__
#define __MY_VECTOR_H__
#include <stdexcept>
#include <iostream>
using namespace std;
template<class T>
class myvector
{
private:
        T *mBegin;
        int mSize;
        int mCapacity;
        static const int EXTEND = 4;
public:
        myvector():mBegin(0),mSize(0),mCapacity(0){}
        myvector(int size):mSize(size),mCapacity(EXTEND+mSize)
        {
                mBegin = new T[mCapacity];
        }
        myvector( T *array, int size ):mSize(size),mCapacity(EXTEND+mSize)
        {
                mBegin = new T[mCapacity];
                for( int i = 0; i != mSize; i ++ )
                {
                        mBegin[i] = array[i];
                }
        }
        myvector( const myvector &another )
        {
                mSize = another.mSize;
                mCapacity = EXTEND+mSize;
                mBegin = new T[mCapacity];
                for( int i = 0; i != mSize; i ++ )
                {
                        mBegin[i] = another.mBegin[i];
                }
        }
        ~myvector()
        {
                delete [] mBegin;
                mBegin = 0;
                mSize = 0;
                mCapacity = 0;
        }
        // ===================================================================
        // member funtions

        // return current size
        int size()
        {
                return mSize;
        }
        // return current capacity
        int capacity()
        {
                return mCapacity;
        }
        // return true if empty
        bool empty()
        {
                if(mSize <= 0)
                        return true;
                else
                        return false;
        }
        // resize the vector, size should be at least 0
        void resize(int size)
        {
                if( size < 0 )
                {
                        std::cerr<<"error : size should be at least 0.";
                        throw ("error : size should be at least 0.");
                }
                else
                {
                        if(size >= mSize)
                        {
                                if(size >= mCapacity)
                                {
                                        if(size > INT_MAX-EXTEND)
                                                throw overflow_error("overflow");
                        
                                        mCapacity = EXTEND+size;
                                        T *temp = new T[mCapacity];
                                        for( int i = 0; i != mSize; i ++ )
                                        {
                                                temp[i] = mBegin[i];
                                        } 
                                        delete [] mBegin;
                                        mBegin = temp;
                                        mSize = size;
                                }
                                else
                                {
                                        mSize = size;
                                }
                        }
                        else
                        {// smaller than mSize
                                /*if(size > INT_MAX-EXTEND)
                                        throw overflow_error("overflow");
                                
                                mCapacity = EXTEND+size;
                                T *temp = new T[mCapacity];
                                for( int i = 0; i != size; i ++ )
                                {
                                        temp[i] = mBegin[i];
                                } 
                                delete [] mBegin;
                                mBegin = temp;*/
                                mSize = size;
                        }
                }
        }
        // change capacity, capacity should be no smaller than size
        void reserve(int capacity)
        {
                if(capacity<mSize)
                {
                        std::cerr<<"error : the capacity is smaller than size.";
                        throw ("error : the capacity is smaller than size.");
                }
                else
                {
                        mCapacity = capacity;
                        T *temp = new T[mCapacity];
                        for( int i = 0; i != mSize; i ++ )
                        {
                                temp[i] = mBegin[i];
                        } 
                        delete [] mBegin;
                        mBegin = temp;
                }
        }
        // return a reference of element at index position
        T& at( int index )
        {
                if( index <0 || index >= mSize )
                {
                        std::cerr<<"error : index out of range.";
                        throw std::out_of_range("error : index out of range.");
                }
                return mBegin[index];
        }
        // return a reference of element at index position
        T& operator[]( int index )
        {
                return at(index);
        }
        // assign a element at index position
        void assign(int index, T value)
        {
                if( index <0 || index >= mSize )
                {
                        std::cerr<<"error : index out of range.";
                        throw std::out_of_range("error : index out of range.");
                }
                else
                {
                        mBegin[index] = value;
                }
        }
        // add element at the end of the vector
        void push_back(T element)
        {
                if(mSize == INT_MAX)
                        throw overflow_error("overflow");
                mSize ++;
                if( mCapacity <= mSize )
                {
                        if(mSize > INT_MAX-EXTEND)
                                throw overflow_error("overflow");
                        mCapacity = mSize + EXTEND;
                        T *temp = new T[mCapacity];
                        for( int i = 0; i != (mSize-1); i ++ )
                        {
                                temp[i] = mBegin[i];
                        } 
                        delete [] mBegin;
                        mBegin = temp;
                }
                mBegin[mSize-1] = element;
        }
        // delete element at the end, return its value 
        T pop_back()
        {
                if( mSize <= 0 )
                {
                        std::cerr<<"error : the vector is empty.";
                        throw ("error : the vector is empty.");
                }
                else
                {
                        mSize --;
                        return mBegin[mSize];
                }
        }
        // insert a element at index position
        void insert(int index, T element)
        {
                if( index <0 || index > mSize )
                {
                        std::cerr<<"error : index out of range.";
                        throw std::out_of_range("error : index out of range.");
                }
                else
                {
                        if(mSize == INT_MAX)
                                throw overflow_error("overflow");
                        mSize ++;
                        if( mCapacity <= mSize )
                        {
                                if(mSize > INT_MAX-EXTEND)
                                        throw overflow_error("overflow");
                                mCapacity = EXTEND + mSize;
                        }
                        

                        T *temp = new T[mCapacity];
                        for( int i = 0; i!= index; i ++ )
                        {
                                temp[i] = mBegin[i];
                        }
                        temp[index] = element;
                        for( int i = index; i != mSize-1; i ++ )
                        {
                                temp[i+1] = mBegin[i];
                        }
                        delete [] mBegin;
                        mBegin = temp;
                }
        }
        // delete element at index position
        void erase(int index)
        {
                if( index <0 || index >= mSize )
                {
                        std::cerr<<"error : index out of range.";
                        throw std::out_of_range("error : index out of range.");
                }
                else
                {
                        mSize --;
                        T *temp = new T[mCapacity];
                        for(int i = 0; i != index; i ++)
                        {
                                temp[i] = mBegin[i];
                        }
                        for(int i = index+1; i != mSize+1; i ++)
                        {
                                temp[i-1] = mBegin[i];
                        }
                        delete [] mBegin;
                        mBegin = temp;
                }
        }
        // remove elements in range [first, last)
        // first included, last not included, first should be smaller than last
        void erase(int first, int last)
        {// [first, last)
                if( first <0 || first >= mSize || last <0 || last > mSize )
                {
                        std::cerr<<"error : index out of range.";
                        throw std::out_of_range("error : index out of range.");
                }
                else
                {
                        // check if first is smaller than last
                        if( first > last )
                        {
                                std::cerr<<"error : first should smaller than last.";
                                throw ("error : first should smaller than last.");
                        }
                        
                        // removing
                        int remove = last-first;
                        //T *temp = new T[(mSize-remove)+10];
                        T *temp = new T[mCapacity];
                        for(int i = 0; i!= first; i ++)
                                temp[i] = mBegin[i];
                        // if last == mSize, delete [] element after first, include first
                        for(int i = last; i != mSize; i ++)
                                temp[i-remove] = mBegin[i];
                        mSize -= remove;
                        delete [] mBegin;
                        mBegin = temp;
                }
        }
        // delete all elements, clean the vector to empty
        void clear()
        {
                delete [] mBegin;
                mBegin = 0;
                mSize = 0;
                mCapacity = 0;
        }

		bool GetArray( T *a, int size){
			if(size != mSize)
				return false;
			for(int i = 0; i != size; i++){
				a[i] = mBegin[i];
			}
			return true;
		}
};
#endif // !__MY_VECTOR_H__