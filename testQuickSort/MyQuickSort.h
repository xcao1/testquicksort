#ifndef __MY_QUICK_SORT__
#define __MY_QUICK_SORT__
// =========================================================

template <class DataType>
class MyQuickSort{
	// [first, last]
	static int Partition(DataType *in_array, int first, int last);
	// [first, last]
	static bool QuickSort(DataType *in_array, int first, int last);
public:
	static bool QuickSortAnArray(DataType *in_array, int size);
};

template <class DataType>
int MyQuickSort<DataType>::Partition(DataType *in_array, int first, int last){
	DataType judge = in_array[last];
	int i = first-1;
	for(int j = first; j != last; j++){
		if(in_array[j]<judge){
			i++;
			DataType temp = in_array[j];
			in_array[j] = in_array[i];
			in_array[i] = temp;
		}
	}
	DataType temp = in_array[last];
	in_array[last] = in_array[i+1];
	in_array[i+1] = temp;
	return i+1;
}

template <class DataType>
bool MyQuickSort<DataType>::QuickSort(DataType *in_array, int first, int last){
	if(first < last){
		int par = Partition(in_array, first, last);
		QuickSort(in_array, first, par-1);
		QuickSort(in_array,par+1,last);
		return true;
	}else{
		if(first == last)
			return true;
		else
			return false;
	}
}

template <class DataType>
bool MyQuickSort<DataType>::QuickSortAnArray(DataType *in_array, int size){
	if(QuickSort(in_array,0,size-1))
		return true;
	else
		return false;
}

// =========================================================
#endif // !__MY_QUICK_SORT__
